#include<iostream>
#include<string>
#include<fstream>
#include<cstdio>
#include<cmath>
#include<iomanip>
#include<cstdlib>
#include"decl.h"
#include"random.h"
using namespace std;

int     pop_size,gen_no,max_gen,no_xover,no_mutation,num_var,num_bin_var,num_obj,lchrom[MAXVECSIZE],chromsize,REPORT,BINGA,FITNESS,PARAM,minmax[MAXOBJ],tourneylist[MAXPOPSIZE],  var_type[MAXVECSIZE],TotalChromLength;       
float   seed,p_xover,p_mutation,x_lower[MAXVECSIZE],x_upper[MAXVECSIZE],afmin[MAXOBJ],afmax[MAXOBJ],dshare,maxf[MAXOBJ],minf[MAXOBJ],        avgfitness[MAXOBJ],dum_avg, min_dum,init_dum,delta_dum,c1=0.0,c2=0.0,weightage[MAXOBJ];      

POPULATION oldpop, newpop;  /* Old and New populations   */

int *choices, nremain;
float *fraction;

// subroutine for inputting global parameters :
void input_parameters()
{
  int   k,temp2,count;
  char  ans;
  float temp1, sumweight;
  
  cout<<"\nNumber of objective functions (2) --> ";
  cin>>num_obj;
  cout<<"\nSpecify minimization (1) or maximization (-1) for each function ";
  for (count=0; count<num_obj; count++)
  {
    cout<<"\n  Objective function #"<<setw(2)<<count+1<<" (1 or -1)      --> "; 
    cin>>minmax[count];
  }
  
  cout<<"\nNumber of variables (Maximum "<<MAXVECSIZE<<") --- : ";
  cin>>num_var;
  
  BINGA = FALSE;
  TotalChromLength=0;
  
  num_bin_var=0;   
  for (k=0; k<= num_var-1; k++)
  {
    var_type[k]=1;
    
    cout<<"\nLower and Upper bounds of x["<<k+1<<"<<] ----- : ";
    cin>>x_lower[k]>>x_upper[k];
    
    num_bin_var++;
    BINGA = TRUE;
    cout<<"\nChromosome Length ------------------ : ";
    cin>>lchrom[k];
    TotalChromLength += lchrom[k];
  }  
  cout<<"\n";
  
  for (count=0;count<num_var;count++)
  {
    cout<<"Variable (#"<<setw(2)<<count+1<<") Type : ";
    cout<<"Lower Bound   : "<<x_lower[count]<<"\n";
    cout<<"Upper Bound   : "<<x_upper[count]<<"\n";
  }
  
  FITNESS=PARAM=FALSE;
  cout<<"\n Sharing Strategy :"; 
  cout<<"\n Sharing on Fitness Space   (f)";
  cout<<"\n Sharing on Parameter Space (p)";
  cout<<"\n Give your choice (f/p) p preferable : ";
  do { ans = getchar(); } while (ans!= 'f' && ans !='p');
  if (ans == 'f')   FITNESS = TRUE;
  else                  PARAM   = TRUE;
  
  if (FITNESS)
  {
    cout<<"\nEqual weightage for all objectives (y or n)?: ";
    do { ans = getchar(); } while (ans!= 'y' && ans != 'n');
    if (ans == 'n') 
    {
     for (count=0, sumweight=0.0; count<num_obj; count++)
     {
       cout<<"\n  Weight for objective #"<<setw(2)<<count+1<<" (0.5) -> ";
       cin>>weightage[count];
       sumweight += weightage[count];
       cout<<"\n  Lower and Upper values of function #"<<setw(2)<<count+1<<" (approx.) -> ";
       cin>>afmin[count]>>afmax[count];
     } 
     for (count=0; count<num_obj; count++)
       weightage[count] /= sumweight; 
   }
   else 
     for (count=0; count<num_obj; count++)
       weightage[count] = 1.0/(double)num_obj;
   }

   dshare=0.5*pow(0.1,1.0/num_var);
   
   cout<<"\nReports to be printed ----- ? (y/n)  : ";
   do { ans = getchar(); } while (ans!= 'y' && ans !='n');
   if (ans == 'y') REPORT = TRUE;
   else            REPORT = FALSE;
   
   cout<<"\nPopulation Size ? (~ 20 times N) ---- : ";
   cin>>pop_size ; 
   if (pop_size > MAXPOPSIZE) 
   {
    cout<<"\n Increase the value of MAXPOPSIZE in program";
    cout<<"  and re-run the program";
    exit(-1);
  }
  
  cout<<"\nCross Over Probability ? ( 0.7 to 1 )  : ";
  cin>>p_xover;
  
  cout<<"\nMutation Probability ? ( 0 to 0.2 ) -- : ";
  cin>>p_mutation;
  
  
  
  cout<<"\nHow many generations (100) ? --------- : ";
  cin>>max_gen;
  
  cout<<"\nGive random seed (0 to 1.0)          : ";
  cin>>seed;
  
}

//  Initialses zero'th generation and global parameters
void initialize()
{
  float u;
  int tmp,k,k1,i,j,j1,stop;
  double temp[MAXVECSIZE],coef;
  unsigned mask=1,nbytes;
  
  randomize(seed);
  oldpop=new INDIVIDUAL[pop_size];
  newpop=new INDIVIDUAL[pop_size];
  if (oldpop == NULL) nomemory("oldpop in initialize()");
  if (newpop == NULL) nomemory("newpop in initialize()");
  
  chromsize = (TotalChromLength/UINTSIZE); 
  if (TotalChromLength%UINTSIZE) chromsize++; 
  nbytes = chromsize*sizeof(unsigned); 
  
  for(j = 0; j < pop_size; j++) 
  { 
   if((oldpop[j].chrom = new unsigned [nbytes])== NULL) 
    nomemory("oldpop chromosomes"); 
  
  if((newpop[j].chrom = new unsigned [nbytes])== NULL) 
    nomemory("newpop chromosomes"); 
} 

for (k=0; k<= pop_size-1; k++) 
{  
  oldpop[k].flag = 0;
  oldpop[k].parent1 = oldpop[k].parent2 = 0;
  oldpop[k].dumfitness = 0.0;
  
      /* Initializing chromosomes for binary variables     */
  for(k1 = 0; k1 < chromsize; k1++) 
  {
   oldpop[k].chrom[k1] = 0; 
   if(k1 == (chromsize-1)) 
    stop = TotalChromLength - (k1*UINTSIZE); 
  else 
    stop = UINTSIZE; 
    /* A fair coin toss */ 
  for(j1 = 1; j1 <= stop; j1++) 
  { 
   if (flip(0.5)) 
    oldpop[k].chrom[k1] = oldpop[k].chrom[k1]|mask; 
  if (j1 != stop) 
   oldpop[k].chrom[k1] = oldpop[k].chrom[k1]<<1; 
}                 
}                    
}                       

no_xover = no_mutation = 0;

  init_dum  = pop_size;             /* For Sharing */
min_dum   = 0.0;
delta_dum = 0.1*init_dum;

  /* Decoding binary strings */
for (k=0; k<= pop_size-1; k++)    
{                                    
  decode_string(&(oldpop[k]));      
  objective(&(oldpop[k]));          
} 
}

//  Decodes the string of the individual (if any) and puts the values in  the array of floats.
void decode_string(INDIVIDUAL *ptr_indiv)
{
  double *temp,coef[MAXVECSIZE]; 
  int j;
  
  if (ptr_indiv == NULL) error_ptr_null("ptr_indiv in decode_string");
  if (BINGA)
  {
    temp = new double[num_var];
    
    for(j=0; j<num_var; j++) 
     temp[j] = 0.0;
   
   decodevalue(ptr_indiv->chrom,temp);
   
   for(j=0; j<num_var; j++) 
     if (var_type[j]==BIN)
     {
       coef[j] = pow(2.0,(double)(lchrom[j])) - 1.0;
       temp[j] = temp[j]/coef[j];
       ptr_indiv->x[j] = temp[j]*x_upper[j] + (1.0 - temp[j])*x_lower[j];
     }
     free(temp);
   }
 }

//  prints an error message and terminates the program
 void nomemory(const char *str) 
 { 
  cout<<"\nmalloc: out of memory making "<<str<<"!!\n"; 
  cout<<"\n Program is halting .....";
  exit(-1); 
} 

//  gives error message of null pointer  and terminates the program. 
void error_ptr_null(const char *str)
{
  cout<<"\n Error !! Pointer "<<str<<" found Null !";
  cout<<"\n Program is halting .....";
  exit(-1);
}

//  calculates statistics of current generation :
void statistics(POPULATION pop,int gen)
{
  int j,iobj;
  float dumsumfit = 0.0;
  float sumfitness[MAXOBJ];
  
  for (iobj=0; iobj<num_obj; iobj++)
  {
    minf[iobj] = maxf[iobj] = 1.0e8;
    sumfitness[iobj] = 0.0;
  }
  
  for (j=0; j<=pop_size-1; j++)
  {
    dumsumfit   += pop[j].dumfitness;
    for (iobj=0; iobj<num_obj; iobj++)
    {
     sumfitness[iobj] += pop[j].fitness[iobj];
     if (pop[j].fitness[iobj] > maxf[iobj]) maxf[iobj] = pop[j].fitness[iobj];
     if (pop[j].fitness[iobj] < minf[iobj]) minf[iobj] = pop[j].fitness[iobj];
   }
 }
 
 dum_avg = dumsumfit/(double)pop_size;
 for (iobj=0; iobj<num_obj; iobj++)
  avgfitness[iobj] = sumfitness[iobj]/(double)pop_size;

}

  //Decodes the value of a group of binary strings and puts the decoded values into an array 'value'.
void decodevalue(unsigned *chrom,double *value) 
{ 
  int temp,k,count,tp,mask=1,sumlengthstring,stop,j,PrevTrack;
  int bitpos,flag1,flag2;
  int CountTrack;
  double  bitpow; 
  
  if (BINGA == FALSE) return;
  if (chrom == NULL) error_ptr_null("chrom in decodevalue");
  
  for(count=0;count<num_var;count++)
    value[count]=0; 
  
  for(k = 0; k < chromsize ; k++) 
  { 
    if (k == (chromsize-1))
     stop = TotalChromLength-(k*UINTSIZE);
   else
     stop = UINTSIZE;
      /* loop thru bits in the current byte */ 
   tp = chrom[k]; 
   for (j=0;j<stop;j++)
   {
     bitpos=j+k*UINTSIZE;
          /* test for current bit 0 or 1 */ 
     if((tp&mask) == 1) 
     {
       sumlengthstring=0;
       flag1=FALSE;
       flag2=FALSE;   
       for(count=0;count<num_var;count++)        
       {                                           
        if ((var_type[count]==BIN)&&(!flag2))  
        {                                     
          if (bitpos>=sumlengthstring)     
           flag1=TRUE;
         else
           flag1=FALSE;
         sumlengthstring+=lchrom[count];
         if ((bitpos<sumlengthstring)&&(flag1)) 
         {  
           flag2=TRUE;
           CountTrack=count;
           PrevTrack=sumlengthstring-lchrom[count];
         }                      
       }                        
     }                                         
     bitpow = pow(2.0,(double)(bitpos-PrevTrack));
     value[CountTrack] += bitpow;
   }                      
   tp = tp>>1; 
 }                       
}                          
} 

  //generation of new population through selection, xover & mutation :
void generate_new_pop()
{
  int j,k,k1,mate1,mate2;
  
  preselect();
  for (k=0; k<= pop_size-1; k += 2)
  {
      mate1 = select();       /* Stoc. Rem. Roulette Wheel Selection */
    mate2 = select();
    
    cross_over_1_site(mate1,mate2,k,k+1); 
    
      mutation(&newpop[k]);        /* Mutation */
    decode_string(&(newpop[k]));
    objective(&(newpop[k]));
    newpop[k].parent1 = mate1+1;
    newpop[k].parent2 = mate2+1;
    
    mutation(&newpop[k+1]);
    decode_string(&(newpop[k+1])); 
    objective(&(newpop[k+1]));
    newpop[k+1].parent1 = mate1+1;
    newpop[k+1].parent2 = mate2+1;
    }                          /* For whole population */
  }


  //binary cross over routine.
  void binary_xover (unsigned *parent1,unsigned  *parent2,unsigned  *child1,unsigned  *child2) 
  { 
    int j, jcross, k; 
    unsigned mask, temp; 
    
    if (BINGA == FALSE) return;
    
  jcross = rnd(1 ,(TotalChromLength - 1));/* Cross between 1 and l-1 */ 
    for(k = 1; k <= chromsize; k++) 
    { 
      if(jcross >= (k*UINTSIZE)) 
      { 
        child1[k-1] = parent1[k-1]; 
        child2[k-1] = parent2[k-1]; 
      } 
      else if((jcross < (k*UINTSIZE)) && (jcross > ((k-1)*UINTSIZE))) 
      { 
       mask = 1; 
       for(j = 1; j <= (jcross-1-((k-1)*UINTSIZE)); j++) 
       { 
        temp = 1; 
        mask = mask<<1; 
        mask = mask|temp; 
      } 
      child1[k-1] = (parent1[k-1]&mask)|(parent2[k-1]&(~mask)); 
      child2[k-1] = (parent1[k-1]&(~mask))|(parent2[k-1]&mask); 
    } 
    else 
    { 
     child1[k-1] = parent2[k-1]; 
     child2[k-1] = parent1[k-1]; 
   } 
 } 
} 


//cross over using strategy of 1 cross site with swapping .
void cross_over_1_site(int first,int second,int childno1,int childno2)
{
  int j,k,site;
  
    if (flip(p_xover))   /* Cross over has to be done */
  {
   no_xover++;
   if (BINGA)
     binary_xover(oldpop[first].chrom   ,oldpop[second].chrom,newpop[childno1].chrom,newpop[childno2].chrom); 
 }                 

 else              
 {   
  if (BINGA)
    for (k=0; k<=chromsize; k++)
    {
     newpop[childno1].chrom[k] = oldpop[first].chrom[k];
     newpop[childno2].chrom[k] = oldpop[second].chrom[k];
   }
   for (k=0; k<=num_var-1; k++)
   {
     newpop[childno1].x[k] = oldpop[first].x[k];
     newpop[childno2].x[k] = oldpop[second].x[k];
   }
 }
}


//binary mutation routine 
void binmutation(unsigned *child) 
{ 
  int j, k, stop; 
  unsigned mask, temp = 1; 
  
  if (BINGA == FALSE) return;
  if (child== NULL) error_ptr_null(" child in binmutation");
  for(k = 0; k < chromsize; k++) 
  { 
    mask = 0; 
    if(k == (chromsize-1)) 
      stop = TotalChromLength - ((k-1)*UINTSIZE); 
    else 
      stop = UINTSIZE; 
    for(j = 0; j < stop; j++) 
    { 
      if(flip(p_mutation)) 
      { 
        mask = mask|(temp<<j); 
      } 
    } 
    child[k] = child[k]^mask; 
  } 
} 

void mutation(INDIVIDUAL * indiv)
{

 if (flip(p_mutation))  no_mutation++;
 
 binmutation(indiv->chrom);
}

  //Reporting the user-specified parameters :
void initreport(ofstream & fp)
{
 int k, iobj;

 if (fp == NULL) error_ptr_null(" File fp in initreport");
 fp<<"\n=============================================";
 fp<<"\n             INITIAL REPORT                  ";
 fp<<"\n=============================================";
 fp<<"\n";
 fp<<"\n Number of objective functions : "<<setw(2)<<num_obj;
 for (iobj=0; iobj<num_obj; iobj++) 
 {
   fp<<"\n   Objective function #"<<setw(2)<<iobj+1<<" : "; 
   if (minmax[iobj] == 1) fp<<"Minimize";
   else if (minmax[iobj] == -1) fp<<"Maximize";
 }
 fp<<"\n\n CROSSOVER TYPE             : ";
 if (BINGA) fp<<"Binary GA (Single-pt)";
 fp<<"\n                              ";
 fp<<"\n STRATEGY                   : 1 cross - site with swapping";
 fp<<"\n Population size            : "<<pop_size;
 fp<<"\n Total no. of generations   : "<<max_gen;
 fp<<"\n Cross over probability     : "<<setw(6)<<setprecision(4)<<p_xover;
 fp<<"\n Mutation probability       : "<<setw(6)<<setprecision(4)<<p_mutation;
 if (BINGA)
  fp<<"\n String length              : "<<TotalChromLength;
fp<<"\n Number of variables";
fp<<"\n                Binary      : "<<num_bin_var;
fp<<"\n                  TOTAL     : "<<num_var;

fp<<"\n Sigma-share value          : "<<setw(6)<<setprecision(4)<<dshare;
fp<<"\n Sharing Strategy           : ";
if (PARAM) 
  fp<<"sharing on Parameter Space";
else if (FITNESS) 
{
 fp<<"sharing on Fitness Space";
 for (iobj=0; iobj<num_obj; iobj++)
  fp<<"\n Weightage for Obj. Fun. #"<<setw(2)<<iobj+1<<" : "<<setw(6)<<setprecision(4)<<weightage[iobj];
}
fp<<"\n Lower and Upper bounds     :";
for (k=0; k<=num_var-1; k++)
 fp<<"\n   "<<setw(8)<<setprecision(4)<<x_lower[k]<<"   <=   x"<<k+1<<"   <= "<<setw(8)<<setprecision(4)<<x_upper[k];
fp<<"\n=================================================\n";
}

//writes a given string of 0's and 1's puts a `-` between each substring (one substring for one variable) Leftmost bit is most significant bit.
void writechrom(unsigned * chrom,ofstream & fp) 
{ 
  int j, k, stop,bits_per_var,count=0; 
  unsigned mask = 1, tmp; 
  int temp[1000];

  for (j=0;j<1000;j++)
    temp[j]=0;
  if (fp == NULL) error_ptr_null(" File fp in initreport");
  if (BINGA == FALSE) return;
  if (chrom == NULL) error_ptr_null("chrom in writechrom");
  for(k = 0; k < chromsize; k++) 
  { 
    tmp = chrom[k]; 
    if(k == (chromsize-1)) 
      stop = TotalChromLength - (k*UINTSIZE); 
    else 
      stop = UINTSIZE; 
    for(j = 0; j < stop; j++) 
    { 
      if(tmp&mask) 
        temp[j+count*UINTSIZE]=1;
      else 
        temp[j+count*UINTSIZE]=0;
      tmp = tmp>>1; 
    } 
    count++;
  } 

  count=0;
  for(j=0;j<num_var;j++)
  {
   for(k=count+lchrom[j]-1;k>=count;k--)
   {
     fp<<temp[k];
   }
   count+=lchrom[j];
   if (j!=num_var-1) fp<<"_";
 }
} 

//Reporting the statistics of current population ( gen. no. 'num')  
void report(ofstream & fp,int num)
{
  int k,j,iobj; 
  char string[30];

  if (fp == NULL) error_ptr_null(" file fp in report()");
  fp<<"\n======================== Generation # : "<<setw(3)<<num<<" =================================";
  if (BINGA) fp<<"\n  No.   Rank         x   Obj. Fun. Values (f1,f2, etc.) Parents   String";
  else fp<<"\n  No.   Rank         x   Obj. Fun. Values (f1,f2, etc.   Parents  ";
   fp<<"\n-----------------------------------------------------------------------------";
   for (k=0; k<= pop_size-1; k++)
   {
     fp<<"\n "<<setw(3)<<k+1<<".   "<<setw(3)<<oldpop[k].front<<"   ["<<setw(7)<<setprecision(3)<<oldpop[k].x[0]<<" ] ";
     for (j= 1; j<=num_var-1; j++)
       fp<<"\n              ["<<setw(7)<<setprecision(3)<<oldpop[k].x[j]<<" ] ";
     for (j=0;j<num_obj;j++) fp<<" "<<fixed<<setw(12)<<setprecision(4)<<oldpop[k].fitness[j]<<" "; 
       fp<<" ("<<setw(3)<<oldpop[k].parent1<<" "<<setw(3)<<oldpop[k].parent2<<")";
     if (BINGA) 
     {  
       fp<<"     ";
       writechrom(oldpop[k].chrom,fp); 
     }
   }
   fp<<"\n-----------------------------------------------------------------------------";
   fp<<"\nAverage Dummy Fitness = "<<dum_avg;
   for(iobj=0; iobj < num_obj; iobj++)
     fp<<"\nObj. Function #"<<setw(2)<<iobj+1<<": Max. Fitness = "<<setw(8)<<setprecision(3)<<maxf[iobj]<<"  Min. Fitness = "<<setw(8)<<setprecision(3)<<minf[iobj]<<" Avg. Fitness = "<<setw(8)<<setprecision(3)<<avgfitness[iobj];
   fp<<"\nNo. of mutations = "<<no_mutation<<" ;  No. of x-overs = "<<no_xover;

   fp<<"\n=============================================================================";
   fp<<"\n\n";
   
 }

//Reporting the statistics of current population ( gen. no. 'num'):
 void result(ofstream & fp,int num)
 {
  int k,j; 
  char string[30];


  if (fp == NULL) error_ptr_null(" file fp in report()");

  fp<<"\n#============== Generation # : "<<setw(3)<<num<<" ===========================================";
  if (BINGA) 
    fp<<"\n#  No.          x         Obj. Fun. Values (f1,f2,etc.)     String";
  else 
    fp<<"\n#  No.          x         Obj. Fun. Values (f1,f2,etc.)  ";
  fp<<"\n#=============================================================================\n";
  for (k=0; k<= pop_size-1; k++)
  {
   for (j=0; j<num_obj; j++)
     fp<<" "<<fixed<<setw(12)<<setprecision(4)<<oldpop[k].fitness[j]<<" ";
   fp<<"\n";
 }
 fp<<"\n#=============================================================================";
 fp<<"\n\n";
 
}

//Releases the memory for all mallocs
void free_all()
{
  int i; 
  
  for(i = 0; i < pop_size; i++) 
  {   
    delete(oldpop[i].chrom); 
    delete(newpop[i].chrom); 
  } 
  delete(oldpop);
  delete(newpop);
  select_free();
}

//Population Fronts Created Here 
void MakeFronts()
{
  int i,j,front_index,pop_count,ijk, iobj, flagobj;
  
  for(i=0; i<pop_size; i++)                /* initializing */
  {
      oldpop[i].flag = 0;   /* making all indivs. untouched */
    oldpop[i].dumfitness = 0.0;
  }
  
  pop_count = 0;   
  
  front_index = 1; /* first front */
  while(pop_count < pop_size)
  {
    for(j=0; j<pop_size; j++)
    {
    if(oldpop[j].flag == 3) continue;/* already assigned
                a front, do not consider*/
    
    for(i=0; i<pop_size; i++)
    {
        if(i == j) continue; /* one is not compared with the same */
     
        else if(oldpop[i].flag == 3) continue; /* already assigned*/
     
        else if(oldpop[i].flag == 1) continue; /* marked dominated*/
     
        else   /* check for domination */
      {
        flagobj = 0;
        for (iobj=0; iobj<num_obj && !flagobj; iobj++) 
          if(minmax[iobj] * oldpop[j].fitness[iobj] <= minmax[iobj] * oldpop[i].fitness[iobj])
            flagobj = 1;
          if (flagobj == 0)
            { oldpop[j].flag = 1; break; }
        }     
      }                           
    if(oldpop[j].flag == 0)  /* non-dominated solutions */
      {
       oldpop[j].flag = 2 ;
       pop_count++ ;
     }
   }                                 
   if(front_index == 1)
   {                      
     for(i=0; i<pop_size; i++)
       if(oldpop[i].flag == 2) 
       {
        oldpop[i].dumfitness = init_dum;
        oldpop[i].front = front_index;
      }
      
      phenoshare();  
      
      minimum_dum();      
      
    front_index++ ;   /* incremented for next front */
    } 
    
      else {      /* for all other fronts 2, 3, ... */
    for(i=0; i<pop_size; i++)
    if(oldpop[i].flag == 2) /* member of current front */
    {  
     oldpop[i].front = front_index ;
     if(min_dum > delta_dum) 
      oldpop[i].dumfitness = min_dum-delta_dum;
        /* smaller than the smallest dummy fitness in previous front */
    else adjust(front_index);
  }

  phenoshare();

  minimum_dum(); 

  front_index++ ;

}                                

for(i=0; i<pop_size; i++)
{
 if(oldpop[i].flag == 2) oldpop[i].flag =  3;
 
 else if(oldpop[i].flag == 1) oldpop[i].flag = 0;
}

}                                    

}

void adjust(int index)
{      /* jack up the fitness of all solutions assigned in a front to accomodate remaining ones */
int i;
double diff;

diff = 2.0 * delta_dum - min_dum ;
for(i=0; i<pop_size; i++)
  if(oldpop[i].flag == 1 || oldpop[i].flag == 0) continue;
else oldpop[i].dumfitness += diff ;

minimum_dum();
}

  //Sharing in a front.  oldpop.dumfitness is divided by nichecount.
void phenoshare()
{
  
  int i,j;
  float  dvalue,d,nichecount;
  double pow();
  float  distance();
  
  for(j=0; j<pop_size; j++)
  {
    nichecount = 1.0;
    if(oldpop[j].flag == 2)
    {
     for(i=0; i<pop_size; i++)
     {
       if(i == j) continue;
       
       if(oldpop[i].flag == 2)
       {
        d = distance(&(oldpop[j]),&(oldpop[i]));
        if (d < 0.0) d = (-1.0)*d ;
        if (d <= 0.000001) nichecount++ ;
        else if(d < dshare) 
          nichecount += (1.0-(d/dshare))*(1.0-(d/dshare));
      }
    }   
  }    
  oldpop[j].dumfitness /= nichecount ;
  
} 
}

  //distance() returns the phenotypic distance
float  distance(INDIVIDUAL *critter1,INDIVIDUAL *critter2)
{
  int i, iobj;
  double dst;
  
  dst = 0.0;
  if (FITNESS)             /* Sharing on fitness space */
  {
    for(iobj=0; iobj<num_obj; iobj++)
      dst += weightage[iobj]*square(critter1->fitness[iobj] - critter2->fitness[iobj])/square(afmax[iobj]-afmin[iobj]);
  }
  else if (PARAM)         /* Sharing on parameter space */
  {
    for (i=0;i<num_var;i++)
     dst += square(critter1->x[i]-critter2->x[i])/square(x_upper[i]-x_lower[i]);
 }
 dst = sqrt(dst);
 return(dst);
}

void minimum_dum()
{    /* finding the minimum dummy fitness in the current front */
int i;

min_dum = 1000000000.0 ;

for(i=0; i<pop_size; i++)
{
  if(oldpop[i].flag == 2)
  {
   if(oldpop[i].dumfitness < min_dum) min_dum = oldpop[i].dumfitness;
 }
}
}

int main()
{
  int j,k,k1;
  POPULATION  temp; /* A temporary pointer of population  */
  
  input_parameters();
  ofstream fp_out ("result.out", ios_base::out); 
  ofstream fp_report("report", ios_base::out); 
  
  select_memory(); 
  initreport(fp_report);   
  
  gen_no=0;
  initialize();
  MakeFronts();
  statistics(oldpop,gen_no);
  
  if (REPORT) report(fp_report,gen_no);  
  
  cout<<"\n =====================================";
  cout<<"======================================== ";
  cout<<"\n Please Wait ";
  
  for(gen_no = 1; gen_no<=max_gen; gen_no++)
  {
    cout<<"."; if (gen_no%60==0) cout<<"\n";
    fflush(stdout);
    
    generate_new_pop();
    
    temp   = oldpop; 
    oldpop = newpop; 
    newpop = temp  ; 
    
    MakeFronts();
    statistics(oldpop,gen_no);
    
    if (gen_no==max_gen) result(fp_out,gen_no);  
    if(gen_no<=10)
      if (REPORT) report(fp_report,gen_no);  
    };                        /* One GA run is over  */
    cout<<"\n ============================================================================= ";
    
    free_all();
    
    fp_out.close();
    fp_report.close();
    cout<<"\n Results are stored in file 'result.out' ";
    puts("\n O.K Good bye !!!"); 
    return 0;
  }

  void select_memory() 
{  /* allocates auxiliary memory for stochastic remainder selection*/
  
  unsigned nbytes;
  int j;
  
  choices = NULL;
  fraction = NULL;
  nbytes = pop_size*sizeof(int);
  if((choices = new int[nbytes])== NULL)
    nomemory("choices");
  nbytes = pop_size*sizeof(float);
  if((fraction = new float[nbytes])== NULL)
    nomemory("fraction");
}

void select_free()
{
  /* frees auxiliary memory for stochastic remainder selection */
  choices = NULL;
  fraction = NULL;
  free(choices);
  free(fraction);
}

void preselect()
     /* preselection for stochastic remainder method */
{
  int j, jassign, k;
  float expected;
  
  if(dum_avg == 0)
  {
    for(j = 0; j < pop_size; j++) choices[j] = j;
  }
else
{
  j = 0;
  k = 0;
      /* Assign whole numbers */
  do
  {
   expected = (float)((oldpop[j].dumfitness)/dum_avg);
   jassign = (int)expected;
    /* note that expected is automatically truncated */
   fraction[j] = expected - (float)jassign;
   while(jassign > 0)
   {
     jassign--;
     choices[k] = j;
     k++;
   }
   j++;
 }
 while(j < pop_size);
 
 j = 0;
      /* Assign fractional parts */
 while(k < pop_size)
 {
   if(j >= pop_size) j = 0;
   if(fraction[j] > 0.0)
   {
        /* A winner if true */
     if(flip(fraction[j]))
     {
      choices[k] = j;
      fraction[j] = fraction[j] - 1.0;
      k++;
    }
  }
  j++;
}
}
nremain = pop_size - 1;
}

int select()
     /* selection using remainder method */
{
  int jpick, slect;
  
  jpick = rnd(0, nremain);
  slect = choices[jpick];
  choices[jpick] = choices[nremain];
  nremain--;
  return(slect);
}


void objective(INDIVIDUAL *person)
{
/*  Test problem ZDT1
    # of real variables = 30
    # of objectives = 2
    */
  double f1, f2, g, h;
  int i;
  f1 = person->x[0];
  g = 0.0;
  for (i=1; i<30; i++)
  {
    g += person->x[i];
  }
  g = 9.0*g/29.0;
  g += 1.0;
  h = 1.0 - sqrt(f1/g);
  f2 = g*h;
  person->fitness[0] = f1;
  person->fitness[1] = f2;
  return ;
}
