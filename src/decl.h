#define BITS_PER_BYTE 8
#define UINTSIZE      (BITS_PER_BYTE*sizeof(unsigned))
#define EPSILON       1e-6
#define MAXVECSIZE    40 
#define MAXPOPSIZE    1000
#define MAXENUMDATA   100
#define MAXOBJ        5
#define MINSCHEME     1   
#define TRUE          1   
#define FALSE         0   
#define ONESITE       1   
#define BIN           1   
#define square(x)  ((x)*(x)) 
#define PENALTY_COEFF 1.0e3

class INDIVIDUAL 
{  
public:
  unsigned *chrom;    
  float fitness[MAXOBJ];
  float x[MAXVECSIZE];
  float dumfitness; 
  int   flag;                   /* flag as follows
                  0 => untouched
                  1 => dominated
                        2 => non-dominated
                  3 => exhausted   */
                        int   front;                  
                        int   parent1;               
                        int   parent2;  
                      };
typedef INDIVIDUAL *POPULATION ;  /* array of individuals */


                      void input_parameters();
                      void initialize();
                      void decode_string(INDIVIDUAL *);
                      void nomemory(const char *) ;
                      void error_ptr_null(const char *);
                      void statistics(POPULATION ,int );
                      void decodevalue(unsigned *,double *) ;
                      void generate_new_pop();
                      void binary_xover (unsigned *,unsigned  *,unsigned  *,unsigned  *) ;
                      void cross_over_1_site(int ,int ,int ,int );
                      void binmutation(unsigned *) ;
                      void mutation(INDIVIDUAL * );
                      void initreport(FILE *);
                      void writechrom(unsigned * ,FILE * ) ;
                      void report(FILE * ,int );
                      void result(FILE *,int );
                      void free_all();
                      void MakeFronts();
                      void adjust(int );
                      void phenoshare();
                      float  distance(INDIVIDUAL *,INDIVIDUAL *);
                      void minimum_dum();
                      void select_memory() ;
                      void select_free();
                      void preselect();
                      int select();
                      void reset1();
                      void objective(INDIVIDUAL *);
                      extern "C"
                      {
                        int flip(float);
                        int rnd(int,int);
                        void randomize(float);
                      }
