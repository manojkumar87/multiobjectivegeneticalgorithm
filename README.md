Implementation of Multi-Objective Genetic Algorithm
------------------------------------------------------

Psuedo Code of Algorithm -
Algorithm can be presented in the form of a pseudo-code as given below:

* STEP 1: Create an initial population N (P0) .

* STEP 2: Assign fitness to each chromosome using NSGA .

* STEP 3: Use sharing strategy for maintaining diversity.

* STEP 4: Generate off spring population Qt by using Stochastic Remainder Roulette Wheel Selection and mutation operator.

* STEP 4.1: Stochastic Remainder Roulette Wheel Selection: In fitness proportionate selection, as in all selection methods, the fitness function assigns fitness to possible solutions or chromosomes. This fitness level is used to associate a probability of selection with each individual chromosome. More the fitness more is the number of copies in pool.

* STEP 5: Crossover: Two strings are crossed based on the crossover point (jcross).
In simple Genetic Algorithm, crossover point= jcross=random (1, Length (chromosome)) i.e. a random number in range from 1 to length of chromosome.
In this approach, crossover point=jcross=(f1(x)/f1(x)+f2(x)) * Length (chromosome).

* STEP 6: Mutation: For each bit in sequence, a random number pm (mutation probability) is figured which decides respective bits should be flipped or not.
These processes ultimately result in the next generation population of chromosomes that is different from the initial generation. Generally the average fitness will have increased by this procedure for the population.

* STEP 7: Termination this generational process is repeated until a termination condition has been reached


REFERENCES
----------------------------
* K. Deb, Multi-Objective Optimization Using Evolutionary Algorithms, John-Wiley, Chicheter, 2001.
* [http://www.genetic-programming.org](http://www.genetic-programming.org/)
* [http://attila.stevens-tech.edu/%7Ecyu2/research/ga.html](http://attila.stevens-tech.edu/%7Ecyu2/research/ga.html)

~                                                               
